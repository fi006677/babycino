// Bug D3. >= has lower precedence than &&

class TestBugD3 {
    public static void main(String[] a) {
	System.out.println(new TestD3().f());
    }
}

class TestD3 {

	// when compiling, returns:
	// 'Expected boolean as 1st argument to &&; actual type: int; Context: y&&z'
	// 'Expected boolean as 2nd argument to &&; actual type: int; Context: y&&z'
	// 'Expected int as 2nd argument to >=; actual type: boolean; Context: x>=y&&z'
	// 'Expected int as 1st argument to >=; actual type: boolean; Context: x>=y&&z>=y'
    public int f() {
	int result;
	int x;
	int y;
	int z;
	x = 2;
	y = 1;
	z = 3;
	if (x >= y && z >= y) {
	    result = 1;
	}
	else{
	}
	return result;
    }

}
