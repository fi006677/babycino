// Bug J1. Allow y to be a boolean expression

class TestBugJ1 {
    public static void main(String[] a) {
	System.out.println(new TestJ1().f());
    }
}

class TestJ1 {

    // doesn't compile and returns:
    // 'Assignment of value of type boolean to variable of incompatible type int'
    // 'Context: result+=y;'
    public int f() {
	int result;
	int count;
	boolean y;
	y =	true;
	count = 1;
	while (count < 11) {
	    result += y;
	    count += 1;
	}
	return result;
    }

}
