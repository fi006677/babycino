// Bug D1. Allow comparison of booleans

class TestBugD1 {
    public static void main(String[] a) {
	System.out.println(new TestD1().f());
    }
}

class TestD1 {

	// this method returns two errors, reading:
	// 'Expected int as 1st argument to >=; actual type: boolean'
	// 'Expected int as 2nd argument to >=; actual type: boolean'
    public int f() {
	boolean a;
	boolean b;
	int result;
	a = true;
	b = false;
	// compares two booleans, a and b
	if (a >= b) {
	    result = 1;
	}
	else{
	}
	return result;
    }

}
