// Bug D2. Return false when x equals y

class TestBugD2 {
    public static void main(String[] a) {
	System.out.println(new TestD2().f());
    }
}

class TestD2 {

	// doesn't compile due to 'Exception in thread "main" java.lang.IllegalArgumentException'
    public int f() {
	int result;
	int x;
	int y;
	x = 1;
	y = 1;
	if (x >= y) {
	    result = 1;
	}
	else{
	}
	return result;
    }

}
