// Bug J2. Instead of assigning x + y to x, assign y to x

class TestBugJ2 {
    public static void main(String[] a) {
	System.out.println(new TestJ2().f());
    }
}

class TestJ2 {
	
	// returns 3, the value of y
    public int f() {
	int x;
	int y;
	x = 2;
	y = 3;
	
	x += y;
	
	return y;
    }

}
