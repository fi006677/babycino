// Bug J3. Do not modify x at all.

class TestBugJ3 {
    public static void main(String[] a) {
	System.out.println(new TestJ3().f());
    }
}

class TestJ3 {

	// 5, the addition of 2 and 3, is returned
    public int f() {
	int x;
	int y;
	x = 2;
	y = 3;
	
	x += y;
	
	return x;
    }

}
